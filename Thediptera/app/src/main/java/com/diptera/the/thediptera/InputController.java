package com.diptera.the.thediptera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by Tien on 29.09.2014.
 */
/*
Handles player movements
 */
@SuppressLint("Typos")
public class InputController extends Activity implements SensorEventListener {

    private float accX;
    private float accY;
    private boolean gyro;
    private boolean acc;

    private Sensor sensor;

    public InputController() {
        gyro = false;
        acc = false;

        SensorManager manager = (SensorManager)MenuActivity.mContext.getSystemService(SENSOR_SERVICE);

        // Sett acc as default. Get gyro as secondary
        if(manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            sensor = manager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
            acc = true;
        } else if(manager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
            sensor = manager.getSensorList(Sensor.TYPE_GYROSCOPE).get(0);
            gyro = true;
        }

        manager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
    }


    // Updates player movements
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        if(gyro) {
            accX = sensorEvent.values[1] * -2;          // Adjust X and Y. X was reversed
            accY = sensorEvent.values[0] * 3;
        } else if(acc) {
            accY = sensorEvent.values[1];
            accX = sensorEvent.values[0]- 5;            // Adjust Y, offset starting position
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @SuppressWarnings("WeakerAccess")
    protected float getAccX()  {
        return accX;
    }

    @SuppressWarnings("WeakerAccess")
    protected float getAccY() {
        return accY;
    }

}
