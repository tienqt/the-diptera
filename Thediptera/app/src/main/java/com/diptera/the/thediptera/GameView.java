package com.diptera.the.thediptera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;


/**
 * Created by Tien on 29.09.2014.
 */
// Displays the game
@SuppressLint("Typos")
public class GameView extends View {

    private Game game;
    private static Context mContext;


    public GameView(Context mContext) {
        super(mContext);
        mContext = this.getContext();

        // Wait until the view is ready
        ViewTreeObserver obs = this.getViewTreeObserver();
        obs.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
              game = new Game(getWidth(), getHeight());
            }
        });
    }

    // Draw the game to screen using canvas
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        game.updateBackground();
        game.drawBackground(canvas);

        game.updatePlayer();
        game.drawPlayer(canvas);

        game.updateAndDrawEnemiesAndPoops(canvas);

        // Update game logic: isPlayerDead, life and score
        game.update(canvas);

        this.postInvalidate();
    }

    public Game getGame() {
        return game;
    }


}
