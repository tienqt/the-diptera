package com.diptera.the.thediptera;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by Tien on 29.09.2014.
 */
// https://www.youtube.com/watch?v=jCtf4e7D5_I
/*
    This class manages the sprites(List of bitmaps, current bitmap).
*/
@SuppressLint("Typos")
public class Sprites {

    private ArrayList<Bitmap> bitmaps;
    private int currentFrameIndex = 0;
    private Bitmap currentFrame;
    private int animationTime;
    private long lastTick;

    // Init bitmap and calculate animation time
    public Sprites(ArrayList<Bitmap> bitmaps, int fps) {
        this.bitmaps = bitmaps;
        this.currentFrame = bitmaps.get(0);
        animationTime = 1000 / fps;
        lastTick = 0;
    }

    // Update currentFrame to next bitmap in the list with a delay delta
    // Animates startIndex and endIndex in the bitmap list
    public void update(long delta, int start, int end) {
        long frameTicker = (long) .01;
        if(delta > frameTicker + animationTime) {
            currentFrameIndex++;
            if(currentFrameIndex > end)
                currentFrameIndex = start;
            if(currentFrameIndex >= bitmaps.size()) {
                currentFrameIndex = 0;
            }
            currentFrame = bitmaps.get(currentFrameIndex);
        }
    }

    // Calls previous update method with currentTime to regulate the animation speed
    public void update(int animationDelay, int start, int end) {
        if(lastTick + animationDelay < System.currentTimeMillis()) {
            update(System.currentTimeMillis(), start, end);
            lastTick = System.currentTimeMillis();
        }
    }

    // Return current bitmap
    public Bitmap getCurrentFrame() { return currentFrame; }

}