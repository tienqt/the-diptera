package com.diptera.the.thediptera;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import java.util.ArrayList;

/**
 * Created by Tien on 29.09.2014.
 */
// https://www.youtube.com/watch?v=jCtf4e7D5_I
/*
    Manages position and size of sprite objects
*/
@SuppressLint("Typos")
public class Animation {
    private Sprites sprites;

    private int w;
    private int h;
    private int x;
    private int y;
    private final int FPS = 60;

    // Init everything and set X and Y
    public Animation(ArrayList<Bitmap> bitmaps, int x, int y) {
        this.x = x;
        this.y = y;

        this.sprites = new Sprites(bitmaps, FPS);
        this.w = bitmaps.get(0).getWidth();
        this.h = bitmaps.get(0).getHeight();
    }


    // Init everything excluding X and Y
    public Animation(ArrayList<Bitmap> bitmaps){
        this.sprites = new Sprites(bitmaps,FPS);
        this.w = bitmaps.get(0).getWidth();
        this.h = bitmaps.get(0).getHeight();
    }

    public Sprites getSprites() { return sprites; }

    public int getY() { return y; }
    public int getX() { return x; }
    public int getH() { return h; }
    public int getW() { return w; }

    public void setX(int x) { this.x = x; }

    public void setY(int y) { this.y = y; }
}