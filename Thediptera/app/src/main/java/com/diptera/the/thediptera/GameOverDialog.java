package com.diptera.the.thediptera;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.diptera.the.thediptera.R;

/**
 * Created by Tien on 29.09.2014.
 */
@SuppressLint("Typos")
public class GameOverDialog {

    private Paint paint;

    private int x;
    private int y;
    private int w;
    private int h;

    private Bitmap backgroundBmp;

    private Rect menuButton;
    private Rect restartButton;
    private Rect submitButton;

    private Resources resources;


    // Defines tree "buttons" (Rects to be checked when touching the screen).
    // Their position is relative to the size of the actual window, so they will
    // fit right on all screens:
    public GameOverDialog(int x, int y, int w) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = (int)(w * 0.6);

        paint = new Paint();

        // Load background image for the dialog
        backgroundBmp = Bitmap.createScaledBitmap(
                BitmapFactory.decodeResource(MenuActivity.mContext.getResources(),
                        R.drawable.gameover), w, h, false);

        resources = MenuActivity.mContext.getResources();

        menuButton = new Rect();
        menuButton.top = y + (int)(h*0.66);
        menuButton.left = x + (int)(w*0.14);
        menuButton.bottom = y + (int)(h*0.78);
        menuButton.right = x + (int)(w*0.47);


        restartButton = new Rect();
        restartButton.top = menuButton.top;
        restartButton.left = x + (int)(w*0.53);
        restartButton.bottom = menuButton.bottom;
        restartButton.right = x + (int)(w*0.85);


        submitButton = new Rect();
        submitButton.top = y + (int)(h*0.83);
        submitButton.left = x + (int)(w*0.35);
        submitButton.bottom = y + (int)(h*0.95);
        submitButton.right = x + (int)(w*0.66);


    }

    // Paints the dialog on the passed canvas
    public void paint(Canvas passedCanvas) {
        passedCanvas.drawBitmap(backgroundBmp, x, y, paint);

        paint.setTextSize(h / 6);
        paint.setTextAlign(Paint.Align.CENTER);
        //noinspection AccessStaticViaInstance
        paint.setColor(new Color().rgb(0,0,0));

        passedCanvas.drawText(resources.getString(R.string.gameOver),
                x + (w/2),
                y + (int)(h/2.5) + paint.getTextSize(),
                paint);


        paint.setTextSize(h / 10);
        paint.setTextAlign(Paint.Align.CENTER);
        //noinspection AccessStaticViaInstance
        paint.setColor(new Color().rgb(255,255,255));

        passedCanvas.drawText(resources.getString(R.string.menu),
                menuButton.centerX(),
                menuButton.bottom - paint.getTextSize() / 3,
                paint);

        passedCanvas.drawText(resources.getString(R.string.restart),
                restartButton.centerX(),
                restartButton.bottom - paint.getTextSize() / 3,
                paint);

        passedCanvas.drawText(resources.getString(R.string.submit),
                submitButton.centerX(),
                submitButton.bottom - paint.getTextSize() / 3,
                paint);
    }

    public Rect getMenuBtnRect() {
        return menuButton;
    }

    public Rect getRestartBtnRect() {
        return restartButton;
    }

    public Rect getSubmitButton() { return submitButton; }

}