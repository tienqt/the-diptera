package com.diptera.the.thediptera;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.JsonReader;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;



@SuppressLint("Typos")

public class HighscoreActivity extends Activity {

    private String userName;
    private int score;

    private Parser parser;

    private String sendScoreUrl;
    private String topTenUrl;

    private RelativeLayout r1;

    private TextView menu;
    private TextView highscore;
    private ListView listView;

    private Context context;

    @Override //initialize and retrieve variables like score if it is passed:
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);

        r1 = (RelativeLayout) findViewById(R.id.r11);

        menu = new TextView(this);
        highscore = new TextView(this);
        listView = new ListView(this);

        Intent intent = getIntent();
        score = intent.getExtras().getInt("score");

        context = this;
    }

    @Override //Checks for available net and starts the loading of highscore
    protected void onResume() {
        super.onResume();
        if (checkForNet()){
            loadHighScore();
        }
    }

    @Override //Added to avoid having two activities of HighScore
    protected void onPause() {
        super.onPause();
    }
    //Checks for internett pretty much:
   // http://stackoverflow.com/questions/4238921/detect-whether-there-is-an-internet-connection-available-on-android
    private boolean checkForNet(){
        ConnectivityManager connectivityManager;
        connectivityManager = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo activeNet = connectivityManager.getActiveNetworkInfo();
        if (activeNet != null){
            return true;
        }
        Toast.makeText(getBaseContext(),R.string.notConnected ,Toast.LENGTH_LONG).show();
        return false;
    }
    //The Parser object is used to retrieve and post score, whatever is needed, if
    //the user wants to submit his score he will be asked for a name
    void loadHighScore() {
        parser = new Parser();
        topTenUrl = "http://gtl.hig.no/getjsonScore.php?GameID=10&from=0&num=10";

        try {
            if (score > -1) {
                showUserNameDialog();
            } else {
                parser.execute(new URL(topTenUrl));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }


    // http://developer.android.com/reference/android/util/JsonReader.html
    // http://developer.android.com/reference/java/net/HttpURLConnection.html
    //Creates the input field for the user:
    void showUserNameDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.userName);



        // Set up the input
        final EditText input = new EditText(this);

        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);


        // Set up the buttons
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                        //Retrieves the input, makes sure there are
             @Override  // no spaces and creates the url before sending it
            public void onClick(DialogInterface dialog, int which) {

                userName = input.getText().toString();
                userName = userName.replace(" ", "");
                sendScoreUrl = "http://gtl.hig.no/logScore.php?GameID=10&User=" + userName + "&Score=" + score;

                try {
                    parser.execute(new URL(topTenUrl), new URL(sendScoreUrl));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });     //for the cancel button
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
        //Sends it all to screen function:
    private void toScreen( ArrayList<String[]> highscoreList){
        ArrayAdapter<String> arrayAdapter;
        ArrayList<String> highscores = new ArrayList<String>();
        int i = 0;                                        //Runs through the Arraylist and retrieves
        for(String[] sa : highscoreList) {                //the string arrays for ten of them
            i++;
            if(i <= 10)
                highscores.add(i + ": " + sa[0] + " - " + sa[1] );
            else
                highscores.add(sa[0]);                    //The last box will contain the users pos.
        }
        //For getting the info into the Listview
        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.test_list_item,highscores);

        // http://stackoverflow.com/questions/4605527/converting-pixels-to-dp
        Resources resources = this.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = r1.getHeight() / (metrics.densityDpi / 160f);
        //Setting the text on the buttons
        menu.setText(R.string.menu);
        menu.setX((float) (r1.getWidth() * 0.43));
        menu.setY((float) (r1.getHeight() * 0.730));
        menu.setTextColor(Color.BLACK);

        menu.setTextSize((float)(r1.getHeight()*0.025));
        menu.setOnClickListener(new View.OnClickListener() {
            @Override //If pushed go back to menu
            public void onClick(View v) {
                Intent menuIntent = new Intent(context, MenuActivity.class);
                menuIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(menuIntent);
            }
        });
        //setting the views and their text:
        menu.setTextSize((float)(dp*0.065));
        r1.addView(menu);

        highscore.setText(R.string.highScore);
        highscore.setX((float)(r1.getWidth()*0.35));
        highscore.setY((float) (r1.getHeight() * 0.078));
        highscore.setTextSize((float)(dp*0.085));
        highscore.setTextColor(Color.BLACK);

        r1.addView(highscore);

        listView.setAdapter(arrayAdapter);
        listView.setLayoutParams(new ActionBar.LayoutParams(r1.getWidth()/4, (int)(r1.getHeight()*0.45)));
        listView.setX((float)(r1.getWidth()*0.35));
        listView.setY((float)(r1.getHeight() * 0.23));
        r1.addView(listView);

    }

    //Class for the AsyncTask, need to retrieve/send the information to the DB
    private class Parser extends AsyncTask<URL, Void, ArrayList<String[]> >{
        String str = null;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override //if there are two parameters (URLs), it will first post the score,
        // then download the highscore list. The posted score will be added at the bottom of the list(view)
        protected  ArrayList<String[]> doInBackground(URL... params) {
            ArrayList<String[]> scoreList = null;
            try {
                if(params.length == 2)
                    str = downloadString(params[1]);
                scoreList = downloadHighScoreList(params[0]);
                if(params.length == 2) {
                    scoreList.add(new String[]{"**** Your score ****"});
                    scoreList.add(new String[]{str.split(",")[0]});
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return scoreList;
        }

        @Override //Sends the info back to main thread
        public void onPostExecute( ArrayList<String[]> scoreList ){
            toScreen(scoreList);
        }



        // Executes an URL and returns the string that the website generates
        private String downloadString(final URL url) throws IOException {
            String ret = "";
            BufferedReader in;
            HttpURLConnection urlConnection;

            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    in =  new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line;
                    while ((line = in.readLine()) != null) {
                        ret += line;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ret;
        }

        // Downloads a Json object, parses it and returns a list of high score entries
        // with the highest score first
        private  ArrayList<String[]> downloadHighScoreList(final URL url) throws IOException {
            ArrayList<String[]> res = null;
            InputStream in;
            HttpURLConnection urlConnection;

            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    in = new BufferedInputStream(urlConnection.getInputStream());
                    res = readJsonStream(in);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    urlConnection.disconnect();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return res;
        }

        //The next functions are pretty much only json reader functions and are stolen
        //from:
        //http://developer.android.com/reference/android/util/JsonReader.html
        public ArrayList<String[]> readJsonStream(InputStream in) throws IOException {
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            try {
                return readMessagesArray(reader);
            } finally {
                reader.close();
            }
        }


        public ArrayList<String[]> readMessagesArray(JsonReader reader) throws IOException {
            ArrayList<String[]> messages = new ArrayList<String[]>();

            reader.beginArray();
            while (reader.hasNext()) {
                messages.add(readMessage(reader));
            }
            reader.endArray();
            return messages;
        }

        public String[] readMessage(JsonReader reader) throws IOException {
            String res[] = new String[3];

            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("User")) {
                    res[0] = reader.nextString();
                } else if (name.equals("Score")) {
                    res[1] = reader.nextString();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            return res;
        }


    }

}
