package com.diptera.the.thediptera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;

/**
 * Created by Tien on 29.09.2014.
 */
@SuppressWarnings("WeakerAccess")
@SuppressLint("Typos")
public class Meny extends View {



    Rect start;
    Rect highScore;
    Rect quit;
    Paint paint;
    Bitmap menuBg;


    public Meny(Context context) {
        super(context);
    }
    //Load bitmap and create the rectangles for the onTouch function in the activity
    public void init(Point passedPoint){
        Point point = passedPoint;
        paint = new Paint();
        menuBg = Bitmap.createScaledBitmap(
                BitmapFactory.decodeResource(MenuActivity.mContext.getResources(),
                        R.drawable.meny), point.x, point.y, false);
        start = new Rect();
        start.left =  (int)(point.x*0.39);
        start.top = (int)(point.y*0.30);
        start.right = (int)(point.x*0.61);
        start.bottom = (int)(point.y * 0.43);

        highScore = new Rect();
        highScore.left = start.left;
        highScore.top = (int) (point.y* 0.50);
        highScore.right = start.right;
        highScore.bottom = (int)(point.y*0.63);

        quit = new Rect();
        quit.left = start.left;
        quit.top = (int) (point.y*0.69);
        quit.right = start.right;
        quit.bottom = (int)(point.y*0.82);

    }

    @Override //draws the bitmap to the canvas:
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        canvas.drawColor(Color.BLACK);
        canvas.drawBitmap(menuBg, 0, 0, paint);
        paintDialog(canvas);
    }


    public Rect getStartButton() {
        return start;
    }

    public Rect getQuitButton() {
        return quit;
    }

    public Rect getHighScoreButton() {
        return highScore;
    }
    //Draws the text on the buttons:
    public void paintDialog(Canvas canvas){
        paint.setTextSize( start.top / 5);
        paint.setTextAlign(Paint.Align.CENTER);
        //noinspection AccessStaticViaInstance
        paint.setColor(new Color().rgb(0,0,0));

        canvas.drawText(getResources().getString(R.string.newGame),
                start.centerX() ,
                start.centerY() + start.height()/4 ,
                paint);
        canvas.drawText(getResources().getString(R.string.highScore),
                highScore.centerX() ,
                highScore.centerY() + highScore.height()/4,
                paint);
        //noinspection AccessStaticViaInstance
        paint.setColor(new Color().rgb(255,255,255));
        canvas.drawText(getResources().getString(R.string.quit),
                quit.centerX() ,
                quit.centerY() + quit.height()/4,
                paint);
    }
}