package com.diptera.the.thediptera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.widget.Toast;



//The Menu Activity

@SuppressLint("Typos")
public class MenuActivity extends Activity {

    public static Context mContext;

    private Point point;
    private static Meny meny;
    private boolean choice;

    private Intent startGameIntent;

                //Calls the test for gyro- or accelerometer
                // creates the intent for starting the game
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        testGyroAcc();

        startGameIntent = new Intent(this, GameActivity.class);
        mContext = this;


    }


    @Override //calls the main function for the menu after pause or at launch:
    public void onResume() {
        super.onResume();
        runMenu();
    }

    //Creates the menu view and makes sure the activity is created before running meny.init():
    @SuppressWarnings("WeakerAccess")
    public void runMenu() {
        Display display;
        choice = true;
        point = new Point();
        display = getWindowManager().getDefaultDisplay();
        display.getSize(point);
        meny = new Meny(this);

        ViewTreeObserver obs = meny.getViewTreeObserver();
        obs.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                meny.init(point);
            }
        });
        setContentView(meny);
    }

    //Starts the game
    private void start() {
        choice = false;
        startActivity(startGameIntent);
    }
    //Checks for presses on the button
    public boolean onTouchEvent(MotionEvent event) {
        if (choice) {
            Rect startBtn = meny.getStartButton();
            Rect highScoreBtn = meny.getHighScoreButton();
            Rect quitBtn = meny.getQuitButton();

            if (startBtn.contains((int) event.getX(), (int) event.getY())) {
                start();
            } else if (highScoreBtn.contains((int) event.getX(), (int) event.getY())) {
                Intent highScoreIntent = new Intent(this, HighscoreActivity.class);
                highScoreIntent.putExtra("score", -1);
                highScoreIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(highScoreIntent);
            } else if (quitBtn.contains((int) event.getX(), (int) event.getY())) {
                this.finish();
            }
        }
        return true;
    }
    //tests for gyro or accelerometer and close the game if it cant find it
    private void testGyroAcc() {
        SensorManager manager = (SensorManager)this.getSystemService(SENSOR_SERVICE);


        if(manager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) == null &&
           manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null) {
            Toast.makeText(getBaseContext(), this.getResources().getText(R.string.exit), Toast.LENGTH_LONG).show();
            this.finish();
        }

    }

}
