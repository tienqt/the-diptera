package com.diptera.the.thediptera;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

/**
 * Created by Tien on 29.09.2014.
 */
@SuppressLint("Typos")
public class Game {
    private InputController inputController;

    private Animation player;
    private Animation bg;

    private Vector<Animation> enemies;
    private Vector<Animation> poops;

    private ArrayList<Bitmap> backGround;
    private ArrayList<Bitmap> playerBitmap;
    private ArrayList<Bitmap> hotdogBitmap;
    private ArrayList<Bitmap> poop;

    private int speedFactor;
    private int backgroundSpeed;
    private int enemySpeed;
    private int score;

    private int width;
    private int height;

    private float life;

    private boolean playerDead;
    private boolean increaseSpeed;
    private Animation rightMost;

    private Paint paint;
    private Rect windowRect;

    private GameOverDialog gameOverDialog;


    public Game(int width, int height) {
        this.height = height;
        this.width = width;
        increaseSpeed = false;
        gameOverDialog = new GameOverDialog(width/4, height/5, width / 2);
        hotdogBitmap = new ArrayList<Bitmap>();
        playerBitmap = new ArrayList<Bitmap>();
        backGround = new ArrayList<Bitmap>();
        poop = new ArrayList<Bitmap>();

        paint = new Paint();

        // Load the background file
        backGround.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(MenuActivity.mContext.getResources(), R.drawable.bg), width*2, height, false));

        // Load all the hotdog bitmaps
        hotdogBitmap.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(MenuActivity.mContext.getResources(), R.drawable.snake1), width / 15, (int) (height*0.70), false));
        hotdogBitmap.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(MenuActivity.mContext.getResources(), R.drawable.snake2), width / 15, (int) (height*0.70), false));

        // Load all the player bitmaps
        playerBitmap.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(MenuActivity.mContext.getResources(), R.drawable.flyfly1), width / 15, width/30, false));
        playerBitmap.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(MenuActivity.mContext.getResources(), R.drawable.flyfly2), width / 15, width/30, false));
        playerBitmap.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(MenuActivity.mContext.getResources(), R.drawable.flydead), width / 15, width / 30, false));

        poop.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(MenuActivity.mContext.getResources(), R.drawable.poop1), width / 20, width/25, false));
        poop.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(MenuActivity.mContext.getResources(), R.drawable.poop2), width / 20, width/25, false));


        // Use the bitmaps to create the animations
        player = new Animation(playerBitmap, width/4, height/2);
        bg = new Animation(backGround, 0, 0);

        // Vector containing enemy animations
        enemies = new Vector<Animation>();
        poops = new Vector<Animation>();

        // Get the rectangle of the screen/view:
        windowRect = new Rect();
        windowRect.top = 0;
        windowRect.left = 0;
        windowRect.bottom = height;
        windowRect.right = width;

        inputController = new InputController();

        startNewGame();
    }

    // Run whenever a new game should be started
    private void startNewGame() {
        // Reset variables:
        speedFactor = 15;
        backgroundSpeed = 5;
        enemySpeed = 5;
        score = 0;
        life = 100;
        playerDead = false;
        player.setX(width / 4);
        player.setY(height / 4);

        // Create some new enemies:
        enemies.clear();
        for (int i = 0; i < 5; i++) {
            Animation newEnemy = new Animation(hotdogBitmap);
            newEnemy.setX(-newEnemy.getW());                                   // Set them outside screen to generate random position
            newEnemy.setY(this.height - newEnemy.getH() + 20);                 // Put them on the ground
            enemies.add(newEnemy);
        }
        rightMost = enemies.lastElement();

        // clears and make new poops
        poops.clear();
        for (int i = 0; i < 5; i++) {
            Animation newPoop = new Animation(poop);
            newPoop.setX(-newPoop.getW());                                   // Set them outside screen to generate random position
            newPoop.setY(this.height - newPoop.getH() + 20);                 // Put them on the ground
            poops.add(newPoop);
        }
    }

    // Puts the enemy at a random location, to the right of the
    // right-most enemy and right of the screen. Specify Min and Max separation
    // between the enemy and the right-most enemy
    void repositionEnemy(Animation enemy, Canvas canvas, int min_x_separation, int max_x_separation, int min_y_off, int max_y_off, int enemyIndex) {
        int min = min_x_separation;
        int max = max_x_separation;

        // Random-generator
        Random rand = new Random();

        // Random number from min to max
        int randSeparation = rand.nextInt((max - min) + 1) + min;

        // New position is the rightmost enemy + the generated separation
        int newX = rightMost.getX() + rightMost.getW() + randSeparation;

        min = min_y_off;
        max = max_y_off;
        int randInt = rand.nextInt((max - min) + 1) + min;
        int newY = this.height - enemy.getH() + 10 + randInt;

        // DOnt spawn them inside the screen
        if(newX < width)
            newX += width;

        enemy.setX(newX);
        enemy.setY(newY);

        repositionPoops(canvas, newX, rightMost.getX(), newY, 0, enemyIndex);
    }

    // Checks if two animations have collided or not
    private boolean collision(Animation a1, Animation a2, int margin) {
        boolean collision = true;
        if ((a1.getY() > (a2.getY() + a2.getH() - margin)))     // Check if a1's top is below a2's bottom
            collision = false;
        if ((a1.getY() + a1.getH()) < a2.getY() + margin)       // Check if a1's bottom is above a2's top
            collision = false;
        if ( a1.getX() > (a2.getX() + a2.getW() - margin))      // Check if a1's left is right of a2's right
            collision = false;
        if ((a1.getX() + a1.getW()) < a2.getX() + margin)       // Check if a1's right is left of a2's left
            collision = false;
        return collision;
    }

    //Reposition coins between two enemies
    private void repositionPoops(Canvas canvas, int maxX, int minX, int maxY, int minY, int enemyIndex) {
        if(enemies.elementAt(enemyIndex).getX() > -50) {
            Random rand = new Random();

            int minnY = enemies.elementAt(enemyIndex).getY();
            int maxxY = height - poops.elementAt(enemyIndex).getH();

            int y = rand.nextInt((maxxY - minnY) + 1) + minnY;
            //                              MAX                                         MIN                                            MIN
            int x = rand.nextInt(((maxX - enemies.firstElement().getW()) - (minX + enemies.firstElement().getW())) + 1) + minX + enemies.firstElement().getW();

            poops.elementAt(enemyIndex).setX(x);
            poops.elementAt(enemyIndex).setY(y);
        }
    }



    // update player animation and movments
    protected void updatePlayer() {
        player.setY(player.getY() + (int)(inputController.getAccX() * speedFactor));
        player.setX(player.getX() + (int)(inputController.getAccY() * speedFactor));

        if(player.getX() + player.getW() > width) {
            player.setX(width - (player.getW()));
        }
        if(player.getX() < 0) {
            player.setX(1);
        }

        if(player.getY() > height - player.getH()) {
            player.setY(height - player.getH());
        }
        if(player.getY() < 0) {
            player.setY(1);
        }

        // Update the player animation
        if(!playerDead)
            player.getSprites().update(50, 0, 1);
    }

    protected void drawPlayer(Canvas canvas) {
        //Draw the player
        canvas.drawBitmap(player.getSprites().getCurrentFrame(), player.getX(), player.getY(), paint);
    }

    // Scrolls the background.
    protected  void updateBackground() {
        bg.setX(bg.getX() - backgroundSpeed);
        if((bg.getX() * -1) >= width) {
            bg.setX(0);
        }
    }

    protected void drawBackground(Canvas canvas) {
        // Draw the background
        canvas.drawBitmap(bg.getSprites().getCurrentFrame(), bg.getX(), bg.getY(), paint);
    }

    // Update collision between player - enemies and player - poops.
    // Update location and repositioning enemies and poops
    protected void updateAndDrawEnemiesAndPoops(Canvas canvas) {
        // Draw all the enemies
        for(int i = 0; i<enemies.size(); i++) {

            // If player hits enemies
            if(collision(player, enemies.get(i), 20)) {
                playerDead = true;
            }

            // Test if player have collected poop
            if (collision(player, poops.elementAt(i), 20)) {
                poops.elementAt(i).setX(-poops.elementAt(i).getW());
                score++;
                life += 10;
                if((score % 10) == 0 && score != 0 ){
                    increaseSpeed = true;
                }
                if(life > 100)
                    life = 100.9f;
            }

            // First, check if the enemy to draw have left the view.
            // If so, move it to the right of the screen again at a random position
            if(enemies.get(i).getX() < -enemies.get(i).getW()) {
                repositionEnemy(enemies.get(i), canvas, player.getW() * 2, width / 3, 0, (int) (height * 0.5), i);
                rightMost = enemies.get(i);
            }

            enemies.get(i).setX(enemies.get(i).getX() - enemySpeed);

            // Animate it
            enemies.get(i).getSprites().update(100, 0, 1);
            // Then draw it
            canvas.drawBitmap(enemies.get(i).getSprites().getCurrentFrame(), enemies.get(i).getX(), enemies.get(i).getY(), new Paint());

            // Draws poop
            poops.elementAt(i).getSprites().update(200, 0, 1);
            poops.elementAt(i).setX(poops.elementAt(i).getX() - enemySpeed);
            canvas.drawBitmap(poops.get(i).getSprites().getCurrentFrame(), poops.get(i).getX(), poops.get(i).getY(), new Paint());
        }
    }

    // update game logic
    protected void update(Canvas canvas) {
        if(life <= 0)
            playerDead = true;
        else
        if(!playerDead)
            life -= 0.1;
        if(increaseSpeed){
            enemySpeed += 1;
            increaseSpeed = false;
        }
        // Stops the game
        if(playerDead) {
            life = 0;
            player.getSprites().update(50, 2,2);
            speedFactor = 0;
            backgroundSpeed = 0;
            enemySpeed = 0;
            player.setY(player.getY() + 20);
            if(player.getY() <= height - player.getH())
                player.setX(player.getX() + 10);

            // Fade background
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.FILL);
            paint.setAlpha(120);
            canvas.drawRect(windowRect, paint);
            paint.setAlpha(255);

            // Show "Game over" dialog:
            gameOverDialog.paint(canvas);
        }

        paint.setTextSize(height / 20);

        paint.setTextAlign(Paint.Align.LEFT);
        canvas.drawText(MenuActivity.mContext.getResources().getString(R.string.health) + " " + (int)life + "%", 0, height / 20, paint);

        paint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText(MenuActivity.mContext.getString(R.string.score) + " " +  score, width, height / 20, paint);
    }

    protected boolean drawGameOverMenu(MotionEvent event) {
        if(playerDead) {
            // If player is dead ("Game over" dialog is visible)
            Rect restartRect = gameOverDialog.getRestartBtnRect();
            Rect menuRect = gameOverDialog.getMenuBtnRect();
            Rect submitRect = gameOverDialog.getSubmitButton();

            // Restart-button touched:
            if (restartRect.contains((int) event.getX(), (int) event.getY())) {
                startNewGame();
            }
            // Menu button touched:
            else if (menuRect.contains((int) event.getX(), (int) event.getY())) {
                return false;
            }
            else if (submitRect.contains((int) event.getX(), (int) event.getY())) {
                // http://stackoverflow.com/questions/10903754/input-text-dialog-android
                Intent startHistory = new Intent(GameActivity.mContext, HighscoreActivity.class);
                startHistory.putExtra("score", score);
                GameActivity.mContext.startActivity(startHistory);
            }
        }
        return true;
    }

}
